# Navi cheatsheet

Navi cheatsheets

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Install, use navi and add my cheatsheet

#### Télécharger navi & le mettre dans tmp
`wget https://github.com/denisidoro/navi/releases/download/v2.20.1/navi-v2.20.1-x86_64-unknown-linux-musl.tar.gz -O /tmp/navi-v2.20.1-x86_64-unknown-linux-musl.tar.gz`

#### Décompresser le tar dans /usr/local/bin
`sudo tar xzf /tmp/navi-v2.20.1-x86_64-unknown-linux-musl.tar.gz -C /usr/local/bin`

#### Vérifier la version de navi
`navi --version`

#### Installer un fuzzy finder (ici 'fzf')
```
sudo wget https://github.com/junegunn/fzf/releases/download/0.38.0/fzf-0.38-linux_amd64.tar.gz -O /tmp/fzf-0.38-linux_amd64.tar.gz \
&& sudo tar xzf /tmp/fzf-0.38.0-linux_amd64.tar.gz -C /usr/local/bin \
&& fzf --version
```

#### Ajouter une cheatsheet (la mienne en l'occurence ici)
`cd $(navi info cheats-path) && git clone https://gitlab.com/nathan.gigonnet.ozenne/navi-cheatsheet`

#### Parcourir navi
`navi`

#### Ajouter un cronjob pour update le sheet au reboot
`crontab -e
@reboot bash -c 'cd "$(navi info cheats-path)/navi-cheatsheet" && git pull'`
## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/nathan.gigonnet.ozenne/navi-cheatsheet.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:0c18c31d17504ac6fbea10f8dcb5d62c?https://docs.gitlab.com/ee/user/clusters/agent/)

***




## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

Ouvert aux contributions.

## Authors and acknowledgment

@nathan.gigonnet.ozenne
@babson4

